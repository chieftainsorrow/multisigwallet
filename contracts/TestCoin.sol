pragma solidity >=0.4.22 <0.8.0;

contract TestCoin {

    string public constant name = "TestCoin";
    string public constant symbol = "TEST";
    uint8 public constant decimals = 18;

    event Transfer(address indexed from, address indexed to, uint tokens);

    mapping(address => uint256) balances;
    uint256 totalSupply_;

    function issueTokens(address _to, uint256 total)
    public
    {
        totalSupply_ = total;
        balances[_to] = totalSupply_;
    }

    function totalSupply() public view returns (uint256) {
        return totalSupply_;
    }

    function balanceOf(address tokenOwner) public view returns (uint) {
        return balances[tokenOwner];
    }

    function transfer(address receiver, uint numTokens) public returns (bool) {
        require(numTokens <= balances[msg.sender]);
        balances[msg.sender] = balances[msg.sender] - numTokens;
        balances[receiver] = balances[receiver] + numTokens;
        emit Transfer(msg.sender, receiver, numTokens);
        return true;
    }
}

