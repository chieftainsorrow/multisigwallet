######  App developed and tested on WSL Ubuntu 20.04

## Run on MacOS 

`npm i`  
`npm run start`  
`npm run deploy:ganache`  
`npm run info:ganache`   
`npm run sendTokens:ganache`  
`npm run submitTransaction:ganache`  
Transaction ID stored in transactionIDs.development.json  
`npm run confirmTransaction:ganache --transactionID #id --addressFrom --privateKey`  
`npm run confirmTransaction:ganache --transactionID #id --addressFrom 0x6704Fbfcd5Ef766B287262fA2281C105d57246a6 --privateKey 2bdd21761a483f71054e14f5b827213567971c676928d9a1808cbfa4b7501201`  
`npm run balanceOf:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`  
Expected address balance: 9000


##  Run on WSL Ubuntu 20.04
`npm i`  
`sudo npm run start-bash`  
`npm run deploy:ganache`  
`npm run info:ganache`   
`npm run sendTokens:ganache`  
`npm run submitTransaction:ganache`  
Transaction ID stored in transactionIDs.development.json  
`npm run confirmTransaction:ganache --transactionID #id --addressFrom --privateKey`  
`npm run confirmTransaction:ganache --transactionID #id --addressFrom 0x6704Fbfcd5Ef766B287262fA2281C105d57246a6 --privateKey 2bdd21761a483f71054e14f5b827213567971c676928d9a1808cbfa4b7501201`  
`npm run balanceOf:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`  

## Testing on Rinkyby testnet using Infura
You need to create .env file. Example of .dotenv located in dotenv_example and you need to set your Infura project ID in rinkeby.config.js in rpcUrl  

`npm run deploy:rinkeby` deploys _ONLY_ wallet and with _one_ owner and _one_ required confirmation for test purposes  
 
`npm run sendTokens:rinkeby --walletAddress #address of deployed wallet# --tokenAddress #address of deployed tokens# --amount #amout of tokens to send#`  

`npm run submitTransaction:rinkeby --addressFrom #from we sending tokens# --privateKey #private key of from Address# --transferTo #address of token reciever# --tokenAddress #token address# --tokenValue #amount of tokens we want to send#`  

`npm run confirmTransaction:rinkeby --transactionID #id of transaction#` because it will be first transaction transactionID will be `0`  


##
## Ganache commands
- ### Start ganache testRPC
    `npm run start`  
    OR if you use WSL Linux 20.04  
    `sudo npm run start-bash`  
    
- ### Stop ganache testRPC  
    `npm run stop`  
    OR if you use WSL Linux 20.04  
    `sudo npm run stop-bash`  
- ### Build 
    `npm run build` 
    OR  
    `sudo npm run build`
    
- ### Delete build folder  
    `npm run clear-build`  
    OR  
    `sudo npm clear-build`
    
- ### Test contracts
    `npm run test`  
    
- ### Deploy contracts to ganache
    `npm run deploy:ganache`
    ###### args:
    - `--totalSupply <type: string>`  
        default: `1000000000000000000000000`  
        example: `npm run deploy:ganache --totalSupply 1000000000000000000000000`
    - `--walletOwners <type: string>`  
        default: `['0xDf08F82De32B8d460adbE8D72043E3a7e25A3B39', '0x6704Fbfcd5Ef766B287262fA2281C105d57246a6', '0x9E1Ef1eC212F5DFfB41d35d9E5c14054F26c6560']`  
        example: `0xe84Da28128a48Dd5585d1aBB1ba67276FdD70776,0xCc036143C68A7A9a41558Eae739B428eCDe5EF66,0xE2b3204F29Ab45d5fd074Ff02aDE098FbC381D42`
    - `--requiredSigs <type: string>`  
        default: `2`  
        example: `--requiredSigs 2`
    ###### command example:
    - `npm run deploy:ganache --totalSupply 1000000000000000000000000  --walletOwners 0xDf08F82De32B8d460adbE8D72043E3a7e25A3B39,0x6704Fbfcd5Ef766B287262fA2281C105d57246a6,0x9E1Ef1eC212F5DFfB41d35d9E5c14054F26c6560  --requiredSigs 2`

- ### Get information about deployed contracts and specific wallet token balance
    `npm run info:ganache`
     ###### args:
     - `--address <type: string>`  
        default: `deploymentAccountAddress`  
        example: `npm run info:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`  
     ###### command example:
    - `npm run info:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`
    
- ### Send tokens
     `npm run sendTokens:ganache`
     ###### optional args:
     - `--address <type: string>`  
         default: `defaultWalletAddress`  
         example: `npm run sendTokens:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`
     - `--amount <type: string>`  
        default: `500000`  
        example: `npm run sendTokens:ganache --amount 142703` 
     ###### command example:
     - `npm run sendTokens:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66 --amount 3333`
- ### Submit transaction
     `npm run submitTransaction:ganache`
     ###### args:
     - `--addressFrom <type: string>`  
         default: `deploymentAccountAddress`  
         example: `npm run submitTransaction:ganache --addressFrom 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`  
     - `--privateKey <type: string>` not buffered key    
        default: `pureDeploymentPrivateKey`  
        example: `npm run submitTransaction:ganache --privateKey 2bdd21761a483f71054e14f5b827213567971c676928d9a1808cbfa4b7501207` 
    - `--transferTo <type: string>`  
        default: `0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`  
        example: `npm run submitTransaction:ganache --transferTo 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66` 
    - `--tokenAddress <type: string>`  
        default: `defaultTokenAddress`  
        example: `0xbbf1aa661d15b50342c4124c3a9c827177e8b6ea`
    - `--tokenValue <type: string>`  
        default: `9000`  
        example: `npm run submitTransaction:ganache --tokenValue 9000` 
     ###### command example:
     - `npm run submitTransaction:ganache --addressFrom 0x26064a2E2b568D9A6D01B93D039D1da9Cf2A58CD --privateKey 2bdd21761a483f71054e14f5b827213567971c676928d9a1808cbfa4b7501206 --transferTo 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66 --tokenValue 9000` 
- ### Confirm transaction
     `npm run confirmTransaction:ganache`
     ###### args:
     - `--transactionID <type: string>`  
         default: `null`  
         example: `npm run confirmTransaction:ganache --transactionID 11`  
     - `--addressFrom <type: string>`  
        default: `deploymentAccountAddress`  
        example: `npm run confirmTransaction:ganache --addressFrom 0xDf08F82De32B8d460adbE8D72043E3a7e25A3B39` 
    - `--privateKey <type: string>`  
        default: `pureDeploymentPrivateKey`  
        example: `npm run confirmTransaction:ganache --privateKey 2bdd21761a483f71054e14f5b827213567971c676928d9a1808cbfa4b7501200` 

     ###### command example:
     - `npm run confirmTransaction:ganache --transactionID 11 --addressFrom 0xDf08F82De32B8d460adbE8D72043E3a7e25A3B39 --privateKey 2bdd21761a483f71054e14f5b827213567971c676928d9a1808cbfa4b7501200` 
- ### Get token balance 
     `npm run balanceOf:ganache`
     ###### args:
     - `--address <type: string>`
        default: `deploymentAccountAddress`  
        example: `npm run balanceOf:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`  
    ###### command example:
     - `npm run balanceOf:ganache --address 0xCc036143C68A7A9a41558Eae739B428eCDe5EF66`