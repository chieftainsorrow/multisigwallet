const env = process.env.ENV || "development";
const {web3, deploymentAccountAddress} = require('../utils/web3');

const Token = require('../build/contracts/FireCoin.json');
const config = require(`../results.${env}.json`);

async function main(argv) {
    console.log(argv, config.tokenContract.address)
    if (!config.tokenContract.address) {
        throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
    }
    const token = new web3.eth.Contract(Token.abi, config.tokenContract.address);
    const tokenMethods = token.methods;
    const balanceOf = await tokenMethods.balanceOf(argv.address).call();

    console.log(`\nToken balance of address ${argv.address}:`);
    console.log(balanceOf);
}

exports.handler = main;
exports.command = 'balanceOf [address]';
exports.describe = 'prints info about deployed contracts and token balance of address';
exports.builder = {
    address:{
        demandOption: false,
        type: 'string',
        default: deploymentAccountAddress
    }
}