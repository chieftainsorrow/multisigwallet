const { deployContract, deploymentAccountAddress } = require('../utils/index')
const {web3} = require('../utils/web3')
const FireCoin = require('../build/contracts/FireCoin.json')
const MultiSigWallet = require('../build/contracts/MultisigWallet.json')
const fs = require('fs')
const w3utils = require('web3-utils');


async function main(argv){
    console.log(argv)
    //for some reason type: array wont work propersly so I decided to pass data as string
    const walletOwners = typeof argv.walletOwners === 'string' ? argv.walletOwners.split(',') : argv.walletOwners;
    const tokenContractOutput = await deployToken(FireCoin, argv.totalSupply);
    const walletContractOutput = await deployWallet(MultiSigWallet, walletOwners, argv.requiredSigs);
    fs.writeFileSync(`./results.${process.env.ENV}.json`, JSON.stringify({
        tokenContract: tokenContractOutput,
        walletContract:walletContractOutput
    }, null, 4))

}
async function deployToken(tokenJSON, totalSupply){
    const nonce = await web3.eth.getTransactionCount(deploymentAccountAddress);
    console.log(`\nDeploying token`)
    console.log(`\nToken nonce: ${nonce}`)
    console.log(`\n[4ireCoin] total supply: ${w3utils.fromWei(totalSupply, 'ether')} 4IRE`);
    const token = await deployContract(tokenJSON, [totalSupply], {from: deploymentAccountAddress, nonce});
    console.log(`\nToken deployed to address:${token.options.address}`);
    return {
        address: token.options.address
    }
}
async function deployWallet(walletJSON, walletOwners, requiredSigs) {
    const nonce = await web3.eth.getTransactionCount(deploymentAccountAddress);
    console.log(`\nDeploying wallet`)
    console.log(`\nWallet nonce: ${nonce}`)
    const wallet =  await deployContract(walletJSON, [walletOwners, requiredSigs], {from: deploymentAccountAddress, nonce})
    console.log(`\nWallet deployed to address:${wallet.options.address}`);
    return {
        address: wallet.options.address
    }
}

exports.handler = main;
exports.command = 'deploy [totalSupply] [walletOwners] [requiredSigs]';
exports.describe = 'deploy token and wallet';
exports.builder = {
    totalSupply: {
        demandOption: false,
        type: 'string',
        default: "1000000000000000000000000"
    },
    walletOwners: {
        demandOption: false,
        type: 'string',
        default: ['0xDf08F82De32B8d460adbE8D72043E3a7e25A3B39', '0x6704Fbfcd5Ef766B287262fA2281C105d57246a6', '0x9E1Ef1eC212F5DFfB41d35d9E5c14054F26c6560']
    },
    requiredSigs: {
        demandOption: false,
        type: 'string',
        default: '2'
    }
}