const env = process.env.ENV || "development";
const {web3, deploymentAccountAddress, deploymentPrivateKey} = require('../utils/web3');
const {sendTx} = require('../utils/index');
const Token = require('../build/contracts/FireCoin.json');
const config = require(`../results.${env}.json`);
const environment = process.env.ENV || 'development';
const defaultWalletAddress = config.walletContract ? config.walletContract.address : '0x0';
const defaultTokenAddress = config.tokenContract ? config.tokenContract.address : '0x0';
    async function main(argv){
        console.log(argv)
    if (!config.tokenContract && environment === 'development') {
        throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
    }
    if (!config.walletContract) {
        throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
    }

    const to = argv.tokenAddress;
    const from = deploymentAccountAddress;
    const privateKey = deploymentPrivateKey;
    const value = 0;

    const token = new web3.eth.Contract(Token.abi, to, {from: deploymentAccountAddress});
    const tokenMethods = token.methods;

    const nonce = await web3.eth.getTransactionCount(deploymentAccountAddress);

    const data =  tokenMethods.transfer(argv.walletAddress, argv.amount).encodeABI();
    const tx = await sendTx({data, nonce, to, privateKey, value, from });
    console.log(`\nTokens transferred to: ${argv.walletAddress}`);
    const balanceOf = await tokenMethods.balanceOf(argv.walletAddress).call();
    console.log(`\nAddress token balance: ${balanceOf}`);
}


exports.handler = main;
exports.command = 'sendTokens [walletAddress] [tokenAddress] [amount]';
exports.describe = 'send tokens to specific address. By default we sending tokens to deployed wallet';
exports.builder = {
    walletAddress: {
        demandOption: false,
        type: 'string',
        default: defaultWalletAddress
    },
    tokenAddress: {
        demandOption: false,
        type: 'string',
        default: defaultTokenAddress
    },
    amount: {
        demandOption: false,
        type: 'string',
        default: 500000
    },
};
