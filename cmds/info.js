const env = process.env.ENV || "development";
const {web3, deploymentAccountAddress} = require('../utils/web3');
const MultiSigWallet = require('../build/contracts/MultisigWallet.json');
const Token = require('../build/contracts/FireCoin.json');
const config = require(`../results.${env}.json`);

async function main(argv) {
  if (!config.tokenContract.address) {
    throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
  }
  if (!config.walletContract.address) {
    throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
  }
  const wallet  = new web3.eth.Contract(MultiSigWallet.abi, config.walletContract.address);
  const walletMethods = wallet.methods;
  const token = new web3.eth.Contract(Token.abi, config.tokenContract.address);
  const tokenMethods = token.methods;
  console.log(`\nWallet:`);
  console.dir(wallet);
  console.log(`\nWallet methods:`);
  console.dir(walletMethods);
  console.log(`\nToken:`);
  console.dir(token);
  console.log(`\nToken methods:`);
  console.dir(tokenMethods);
  const balanceOf = await tokenMethods.balanceOf(argv.address).call();

  console.log(`\nToken balance of address ${argv.address}:`);
  console.log(balanceOf);
}

exports.handler = main;
exports.command = 'info [address]';
exports.describe = 'prints info about deployed contracts and token balance of address';
exports.builder = {
  address:{
    demandOption: false,
    type: 'string',
    default: deploymentAccountAddress
  }
}