const env = process.env.ENV || "development";
const {web3, deploymentAccountAddress, pureDeploymentPrivateKey} = require('../utils/web3');
const {sendTx} = require('../utils/index');
const MultiSigWallet = require('../build/contracts/MultisigWallet.json');
const Token = require('../build/contracts/FireCoin.json');
const config = require(`../results.${env}.json`);
const fs = require('fs');
const w3utils = require('web3-utils');
const environment = process.env.ENV || 'development';
const defaultWalletAddress = config.walletContract ? config.walletContract.address : '0x0';
const defaultTokenAddress = config.tokenContract ? config.tokenContract.address : '0x0';
const deploymentPrivateKey = pureDeploymentPrivateKey ? pureDeploymentPrivateKey : null;
async function main (argv) {
    console.log(argv)
    if (!config.tokenContract && environment === 'development') {
        throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
    }
    if (!config.walletContract  && environment === 'development') {
        throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
    }

    const wallet  = new web3.eth.Contract(MultiSigWallet.abi, config.walletContract.address);
    const walletMethods = wallet.methods;
    const token = new web3.eth.Contract(Token.abi, argv.tokenAddress ); //,{from: config.walletContract.address}
    const tokenMethods = token.methods;

    const to = defaultWalletAddress;
    const privateKey = Buffer.from(argv.privateKey, 'hex');
    const value = '0x0';
    const from = argv.addressFrom;
    const transferTo = argv.transferTo;
    const tokenValue = argv.tokenValue;

    const nonce = await web3.eth.getTransactionCount(from);
    const transfer = await tokenMethods.transfer(transferTo, tokenValue).encodeABI();
    const data = await walletMethods.submitTransaction(argv.tokenAddress, value, transfer).encodeABI();



    const tx = await sendTx({data, nonce, to, privateKey, value, from });
    console.log("\nTransaction receipt");
    console.log(tx);
    const events = await wallet.getPastEvents('Submitted', {fromBlock: 0,  toBlock:"latest"});
    console.log(events)
    if(events && events.length !== 0){
        fs.writeFileSync(`./transactionIDs.${process.env.ENV}.json`, JSON.stringify({
            transactionID: events[events.length - 1].returnValues
        }, null, 4), {flag: 'a+'})
    }


}

exports.handler = main;
exports.describe = 'submitting transaction to transfer tokens';
exports.command = 'submitTransaction [addressFrom] [privateKey] [transferTo] [tokenAddress] [tokenValue]';
exports.builder = {
    addressFrom:{
        demandOption: false,
        type: 'string',
        default:deploymentAccountAddress
    },
    privateKey:{
        demandOption: false,
        type: 'string',
        default:deploymentPrivateKey
    },
    transferTo:{
        demandOption: false,
        type: 'string',
        default:'0xCc036143C68A7A9a41558Eae739B428eCDe5EF66'
    },
    tokenAddress:{
        demandOption: false,
        type: 'string',
        default: defaultTokenAddress
    },
    tokenValue:{
        demandOption: false,
        type: 'string',
        default:'9000'
    }
};