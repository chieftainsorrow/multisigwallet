const { deployContract, deploymentAccountAddress } = require('../utils/index')
const {web3} = require('../utils/web3')
const MultiSigWallet = require('../build/contracts/MultisigWallet.json')
const fs = require('fs')



async function main(argv){
    //for some reason type: array wont work propersly so I decided to pass data as string
    const walletOwners = typeof argv.walletOwners === 'string' ? argv.walletOwners.split(',') : argv.walletOwners;
    const walletContractOutput = await deployWallet(MultiSigWallet, walletOwners, argv.requiredSigs);
    fs.writeFileSync(`./results.${process.env.ENV}.json`, JSON.stringify({
        walletContract:walletContractOutput
    }, null, 4))

}

async function deployWallet(walletJSON, walletOwners, requiredSigs) {
    const nonce = await web3.eth.getTransactionCount(deploymentAccountAddress);
    console.log(`\nDeploying wallet`)
    console.log(`\nWallet nonce: ${nonce}`)
    const wallet =  await deployContract(walletJSON, [walletOwners, requiredSigs], {from: deploymentAccountAddress, nonce})
    console.log(`\nWallet deployed to address:${wallet.options.address}`);
    return {
        address: wallet.options.address
    }
}

exports.handler = main;
exports.command = 'deployWallet [walletOwners] [requiredSigs]';
exports.describe = 'deploy wallet';
exports.builder = {
    walletOwners: {
        demandOption: false,
        type: 'string',
        default: deploymentAccountAddress
    },
    requiredSigs: {
        demandOption: false,
        type: 'string',
        default: '1'
    }
}