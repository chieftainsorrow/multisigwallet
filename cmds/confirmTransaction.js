const env = process.env.ENV || "development";
const {web3, deploymentAccountAddress, pureDeploymentPrivateKey} = require('../utils/web3');
const {sendTx} = require('../utils/index');
const MultiSigWallet = require('../build/contracts/MultisigWallet.json');
const config = require(`../results.${env}.json`);


async function main (argv) {
    if (!config.walletContract) {
        throw new Error(`Token is not deployed. Please deploy it to ${env} first.`);
    }
    if(!argv.transactionID) {
        throw new Error(`Transaction ID is null. Pass transactionID as argument of --transactionID option.`)
    }
    const wallet  = new web3.eth.Contract(MultiSigWallet.abi, config.walletContract.address);
    const walletMethods = wallet.methods;
    const to = config.walletContract.address;

    const from = argv.addressFrom;
    const privateKey = Buffer.from(argv.privateKey, 'hex');
    const value = '0x0'
    const data = await walletMethods.confirmTransaction(argv.transactionID).encodeABI();

    const nonce = await web3.eth.getTransactionCount(from);

    const tx = await sendTx({data, nonce, to, privateKey, value, from });
    console.log(tx)
}

exports.handler = main;
exports.command = 'confirmTransaction  [transactionID] [addressFrom] [privateKey]';
exports.describe = 'confirm transaction';
exports.builder = {
    transactionID:{
        demandOption: false,
        type: 'string',
        default:null
    },
    addressFrom:{
        demandOption: false,
        type: 'string',
        default:deploymentAccountAddress
    },
    privateKey:{
        demandOption: false,
        type: 'string',
        default:pureDeploymentPrivateKey
    }
}