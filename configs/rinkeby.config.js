const utils = require('web3-utils');
require('dotenv').config();
const deploymentAccountAddress = process.env.DEPLOYMENTACCOUNTADDRESS;
const deploymentPrivateKey = process.env.DEPLOYMENTPRIVATEKEY;

exports.deployment = {
    rpcUrl: "https://rinkeby.infura.io/v3/rinkebyProjectID",
    gasLimit: 9000000,
    gasPrice: utils.toWei('50', 'gwei'),
    getReceiptInterval: 100,
    deploymentAccountAddress,
    deploymentPrivateKey: Buffer.from(deploymentPrivateKey, "hex"),
    pureDeploymentPrivateKey: deploymentPrivateKey
}
