const environment = process.env.ENV || 'development';
let deploymentConfig = {};

if (environment === 'development') {
    deploymentConfig = require('../configs/development.config').deployment;
} else if(environment === 'rinkeby') {
    deploymentConfig = require('../configs/rinkeby.config').deployment;
} else {
    throw new Error("Not supported environment");
}
const Web3 = require('web3');
const web3Provider = new Web3.providers.HttpProvider(deploymentConfig.rpcUrl);
const web3 = new Web3(web3Provider);

module.exports = {
    web3,
    ...deploymentConfig
}