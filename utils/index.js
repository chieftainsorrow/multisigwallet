const {
    web3,
    gasPrice,
    gasLimit,
    deploymentPrivateKey,
    deploymentAccountAddress,
    rpcUrl,
    getReceiptInterval
} = require('./web3')
const {Transaction} = require('ethereumjs-tx')
const utils = require('web3-utils');
const fetch = require('node-fetch');
const assert = require('assert');
const environment = process.env.ENV || 'development';
async function deployContract(contractJSON, args, {from, nonce, libraries}) {
    const options = {
        from,
        gasPrice
    }

    let contractBytecode = contractJSON.bytecode;
    //ask later for what it used
    if (libraries !== undefined) {
        for (let key in libraries) {
            const placeholder = `__${key}`+"_".repeat(40-key.length-2);
            contractBytecode = contractBytecode.split(placeholder).join(libraries[key].replace("0x", ""));
        }
    }
    const contractInstance = new web3.eth.Contract(contractJSON.abi, options);
    const data = await contractInstance.deploy({
        data: contractBytecode,
        arguments: args
    }).encodeABI()
    const tx = await sendRawTx({
        data,
        nonce,
        to: null,
        privateKey: deploymentPrivateKey,
    });

    if (tx.status !== '0x1') {
        throw new Error(`Tx failed: ${JSON.stringify(tx)}`);
    }
    contractInstance.options.address = tx.contractAddress;
    contractInstance.deployedBlockNumber = tx.blockNumber;
    return contractInstance;
}


async function sendRawTx({data, nonce, to, privateKey, value}) {
    try{
    //config we passing into Transaction class
    const rawTx = {
        nonce: utils.toHex(nonce),
        gasPrice: utils.toHex(gasPrice),
        gasLimit: utils.toHex(gasLimit),
        to,
        value,
        data
    };
    let txOptions = {}
    if(environment === 'rinkeby'){
        txOptions.chain = 'rinkeby'
    }
    const tx = new Transaction(rawTx, txOptions);
    //sign transaction
    tx.sign(privateKey);
    //buffering tx before we send it into blockchain
    const serializedTX = tx.serialize();
    let time = new Date().getTime();
    const txHash = await sendNodeRequest(rpcUrl, 'eth_sendRawTransaction', `0x${serializedTX.toString('hex')}`);
    console.log(`Request: ${(new Date().getTime() - time)/1000}s`)
    console.log('pending txHash', txHash);
    const receipt = await getReceipt(txHash, rpcUrl);
    console.log(`Receipt: ${(new Date().getTime() - time)/1000}s`)
    return receipt;
    }  catch (err) {
        console.dir(err)
    }
}

async function sendNodeRequest(url, method, signedData) {
    const request = await fetch(url, {
        headers: {
            'Content-type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({
            jsonrpc: '2.0',
            method,
            params: [signedData],
            id: 1
        })
    });
    const json = await request.json();
    if (method === 'eth_sendRawTransaction') {
        if (json.result) {
            assert.equal(json.result.length, 66, `Tx wasn't sent ${json}`);
        } else {
            console.error(`ERROR in tx: ${json.error.message}`);
            throw new Error(json.error.message);
        }
    }
    return json.result;
}

function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
async function getReceipt(txHash, url) {
    await timeout(getReceiptInterval);
    let receipt = await sendNodeRequest(url, 'eth_getTransactionReceipt', txHash);
    if (receipt === null || !receipt.blockNumber) {
        receipt = await getReceipt(txHash, url);
    }
    return receipt;
}
function compareHex(leftHex, rightHex) {
    return parseInt(leftHex, 16) === parseInt(rightHex, 16);
}

async function sendTx({data, nonce, to, privateKey, value, from}){
    try{
        //config we passing into Transaction class
        const rawTx = {
            from,
            to,
            value,
            gasLimit: utils.toHex(gasLimit),
            gasPrice: utils.toHex(gasPrice),
            nonce: utils.toHex(nonce),
            data
        };
        let txOptions = {}
        if(environment === 'rinkeby'){
            txOptions.chain = 'rinkeby'
        }
        const tx = new Transaction(rawTx, txOptions);
        //sign transaction
        tx.sign(privateKey);
        //buffering tx before we send it into blockchain
        const serializedTX = tx.serialize();

        let time = new Date().getTime();
        const txHash = await sendNodeRequest(rpcUrl, 'eth_sendRawTransaction', `0x${serializedTX.toString('hex')}`);
        console.log(`Request: ${(new Date().getTime() - time)/1000}s`)
        console.log('pending txHash', txHash);
        const receipt = await getReceipt(txHash, rpcUrl);
        console.log(`Receipt: ${(new Date().getTime() - time)/1000}s`)
        return receipt;
    }  catch (err) {
        console.dir(err)
    }
}
module.exports = {
    deploymentAccountAddress,
    deployContract,
    sendNodeRequest,
    getReceipt,
    sendRawTx,
    compareHex,
    sendTx
}